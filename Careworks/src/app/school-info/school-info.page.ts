import { Component, OnInit } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { SchoolProfileService, SearchType } from '../services/school-profile.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-school-info',
  templateUrl: './school-info.page.html',
  styleUrls: ['./school-info.page.scss'],
})
export class SchoolInfoPage implements OnInit {

  results: Observable<any>;
  schoolId: String = '';
  constructor(private activatedRoute: ActivatedRoute, private router: Router, private schoolProfileService: SchoolProfileService) { }

  ngOnInit() {
  }

}
