import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SchoolInfoPage } from './school-info.page';

describe('SchoolInfoPage', () => {
  let component: SchoolInfoPage;
  let fixture: ComponentFixture<SchoolInfoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SchoolInfoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SchoolInfoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
