import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SchoolInfoPage } from './school-info.page';

const routes: Routes = [
  {
    path: '',
    component: SchoolInfoPage,
    children: [
        { path: 'school-profile', 
        loadChildren: '../school-profile/school-profile.module#SchoolProfilePageModule' },
        { path: 'student-profile', 
        loadChildren: '../student-profile/student-profile.module#StudentProfilePageModule' },
        { path: 'teachers-profile', 
        loadChildren: '../teachers-profile/teachers-profile.module#TeachersProfilePageModule' },
        { path: 'education-dept', 
        loadChildren: '../education-dept/education-dept.module#EducationDeptPageModule' },
        { path: 'stakeholders-profile', 
        loadChildren: '../stakeholders-profile/stakeholders-profile.module#StakeholdersProfilePageModule' },
        { path: 'other-info', 
        loadChildren: '../other-info/other-info.module#OtherInfoPageModule' },
        { path: 'school-profile/:id', 
        loadChildren: '../school-profile/school-profile.module#SchoolProfilePageModule' }
    ]
  },
  {
    path: '',
    redirectTo: '/school-profile',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SchoolInfoPage]
})
export class SchoolInfoPageModule {}
