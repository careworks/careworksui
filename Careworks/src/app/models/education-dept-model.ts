export class EducationDeptModel{
    hmName: string;
    hmContact: string;
    hmEmail: string;
    hmInChargeName: string;
    hmInChargeContact: string;
    hmInChargeEmail: string;
    crpName: string;
    crpContact: string;
    crpEmail: string;
    brcName: string;
    brcContact: string;
    brcEmail: string;
    beoName: string;
    beoContact: string;
    beoEmail: string;
       
}
