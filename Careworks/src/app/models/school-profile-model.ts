export class SchoolProfileModel{
    name: string;
    establishedYear: string;
    location: string;
    address: string;
    schoolCluster: string;
    block: string;
    district: string;
    state: string;
    category: { [key: string]: string };
    type: { [key: string]: string };
    medium: string; //selector
    diseCode: string;
    emailId: string;
    studentCount: string;
}
