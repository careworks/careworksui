export class StakeholdersProfileModel{
    sdmcTotal: number;
    sdmcYear: string;
    sdmcSpoc: string;
    sdmcContact: string;
    alumniTotal: number;
    alumniYear: string;
    alumniSpoc: string;
    alumniContact: string;
    parentsTeachersTotal: number;
    parentsTeachersYear: string;
    parentsTeachersSpoc: string;
    parentsTeachersContact: string;
    schoolCabinetTotal: number;
    schoolCabinetYear: string;
    schoolCabinetSpoc: string;
    schoolCabinetContact: string;
    pocsoTotal: number;
    pocsoYear: string;
    pocsoSpoc: string;
    pocsoContact: string;   
}
