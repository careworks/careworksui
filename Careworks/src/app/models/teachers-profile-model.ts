export class TeachersProfileModel{
    maleFulltimetotal: number;
    femaleFulltimetotal: number;
    maleParttimetotal: number;
    femaleParttimetotal: number;
    arttotal: number;
    petotal: number;
    comptotal: number;
    totalTeachers: number;      
}
