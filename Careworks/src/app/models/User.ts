export class LoginForm
{
    userNameOrEmail:string;
    password:string;
}

export class SignupForm
{
    name:string;
    username:string;
    email:string;
    password:string;
}
