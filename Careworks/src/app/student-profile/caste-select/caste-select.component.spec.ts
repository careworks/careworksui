import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CasteSelectComponent } from './caste-select.component';

describe('CasteSelectComponent', () => {
  let component: CasteSelectComponent;
  let fixture: ComponentFixture<CasteSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CasteSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CasteSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
