import { Component, OnInit ,Input, EventEmitter, Output} from '@angular/core';


@Component({
  selector: 'app-caste-select',
  templateUrl: './caste-select.component.html',
  styleUrls: ['./caste-select.component.scss']
})
export class CasteSelectComponent implements OnInit {
  @Input() data: any
  @Output() onDelete = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
    this.data.id = new Date().getTime()
  }

  deleteClicked() {
    console.log("deleteClicked")
    this.onDelete.next(this.data)
  }

}
