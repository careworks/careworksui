import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { StudentProfilePage } from './student-profile.page';
import { InputRowComponent } from './input-row/input-row.component';
import { CasteSelectComponent } from './caste-select/caste-select.component';

const routes: Routes = [
  {
    path: '',
    component: StudentProfilePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [StudentProfilePage, InputRowComponent, CasteSelectComponent]
})
export class StudentProfilePageModule {}
