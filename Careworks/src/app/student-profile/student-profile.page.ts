import { Component, OnInit } from '@angular/core';
// import { NavController } from 'ionic-angular';
import { SchoolProfileService } from '../services/school-profile.service';
import { LocalStoreServiceService } from '../services/local-store-service.service';
import { StudentProfileModel } from '../models/student-profile-model'


//IMPORT SUB COMPONENT HERE 
import { InputRowComponent } from './input-row/input-row.component';


@Component({
  selector: 'app-student-profile',
  templateUrl: './student-profile.page.html',
  styleUrls: ['./student-profile.page.scss'],
})
export class StudentProfilePage implements OnInit {

  inputRowValues = [{}]
  x: any;

  class_no: any
  obj: any = {};
  studentPM = new StudentProfileModel();

  parsedJson = null;
  parsedJson1 = null;
  classList = null;
  modified = null;
  mediumList = null;
  casteList = null;
  shouldHideTable: any;
  constructor(private schoolProfileService: SchoolProfileService, private localStoreService: LocalStoreServiceService) {
    // this.x = 0;
    this.obj = this.schoolProfileService.getSampleJson();
    this.class_no = 1;
    this.shouldHideTable = true;
  }

  ngOnInit() {
    this.modified = this.schoolProfileService.getSampleJson();
    if (this.localStoreService.isSearchSchoolFlow) {
      this.shouldHideTable = false;
      this.classList = this.schoolProfileService.getSampleJson().schoolProfileAdvanced.studentDetails[0].details
      this.mediumList = this.schoolProfileService.getSampleJson().schoolProfileAdvanced.studentDetails

      this.x = this.schoolProfileService.getSampleJson().schoolProfileAdvanced.studentDetails[1].details.length
      console.log(this.x)
      console.log(this.mediumList)
    }

  }

  deleteRow(rowClass) {
    // alert(row);

    /// delete part not working 
    
    let i ;
    for(i=0; i < this.modified.schoolProfileAdvanced.studentDetails[1].details.length; i++)
    {
      if(this.modified.schoolProfileAdvanced.studentDetails[1].details[i].class == rowClass)
      {
        delete this.modified.schoolProfileAdvanced.studentDetails[1].details[i];
      }
    }
  }

  addRow() {
    this.inputRowValues.push({})
    this.x += 1;

    this.obj.schoolProfileAdvanced.studentDetails[1].details.push({
      "class": this.studentPM.class,
      "sectionCount": this.studentPM.sectionCount,
      "totalBoys": this.studentPM.totalBoys,
      "totalGirls": this.studentPM.totalGirls,
      "scboysNumber": this.studentPM.scboysNumber,
      "scgirlsNumber": this.studentPM.scgirlsNumber,
      "stboysNumber": this.studentPM.stboysNumber,
      "stgirlsNumber": this.studentPM.stgirlsNumber
  });
     console.log( this.obj.schoolProfileAdvanced.studentDetails[1].details)
     this.modified = this.obj.schoolProfileAdvanced.studentDetails[1].details
    this.class_no += 1;
  }

  onDelete(_event) {
    console.log(_event)
    this.inputRowValues = this.inputRowValues.filter((i: any) => i.id !== _event.id)
  }

}
