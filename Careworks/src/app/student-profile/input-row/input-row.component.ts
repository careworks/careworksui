import { Component, OnInit, Input, EventEmitter, Output} from '@angular/core';
import { CasteSelectComponent } from '../caste-select/caste-select.component';

// IMPORT SUB COMPONENT


@Component({
  selector: 'app-input-row',
  templateUrl: './input-row.component.html',
  styleUrls: ['./input-row.component.scss']
})
export class InputRowComponent implements OnInit {

  @Input() data: any

  inButtonInputRowValue = [{}]

  @Output() onDelete = new EventEmitter<any>();

  constructor() { }

  inClassAddRows() {
    this.inButtonInputRowValue.push({})
  }


  ngOnInit() {
    this.data.id = new Date().getTime()

  }

  deleteClicked() {
    console.log("deleteClicked")
    this.onDelete.next(this.data)
  }

}
