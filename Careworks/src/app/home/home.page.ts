import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { RouterModule, Routes} from '@angular/router';
import {AuthenticationService} from './../services/authentication.service';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { LocalStoreServiceService } from '../services/local-store-service.service';
import { SchoolInfoPage } from '../school-info/school-info.page';
import { SchoolDashboardPage } from '../school-dashboard/school-dashboard.page';
import { AlertController } from '@ionic/angular';
import { SchoolProfileService, SearchType } from '../services/school-profile.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
	schoolInfoPage : SchoolInfoPage;
	showSearchResult: Boolean = false;
  showCancelButton: Boolean = false;
  advancedSearchVisible: Boolean = false;
  
  results: Observable<any>;
  searchTerm: string = '';
  type: SearchType = SearchType.name;

	constructor(private router: Router, public menuCtrl: MenuController, public alertController: AlertController, private schoolProfileService: SchoolProfileService,private authService:AuthenticationService, private localStoreService: LocalStoreServiceService){
		this.menuCtrl.enable(true);
	}

  advancedSearch(){
  this.advancedSearchVisible =! this.advancedSearchVisible;
  }

	navigateSchoolInfo(): void {
    this.localStoreService.isSearchSchoolFlow = false; 
	  this.router.navigateByUrl('/school-info/school-profile');
  }
  
  searchChanged() {
    // Call our service function which returns an Observable
    if(this.searchTerm.length > 2){
      this.results = this.schoolProfileService.searchData(this.searchTerm, this.type);
    }else{
      this.results = null;
    }
  }

	getSearchResults(): void {
		this.showSearchResult  = true;
	}

	onChangeTime(value) {
		if(value != ''){	
		  this.showCancelButton = true;
		}
	}

	onInputTime(value) {
	  if(value === ''){
	  	this.showCancelButton = false;
	  }
	}

	async presentAlertPrompt() {
    const alert = await this.alertController.create({
      header: 'Advanced Search',
      inputs: [
        {
          name: 'School Name',
          type: 'text',
          placeholder: 'School Name'
        },
        {
          name: 'City',
          type: 'text',
          id: 'City-id',
          value: '',
          placeholder: 'City'
        },
        {
          name: 'Region',
          value: '',
          type: 'text',
          placeholder: 'Region'
        },
        {
          name: 'Pin Code',
          value: '',
          type: 'number',
          placeholder: 'Pin Code'
        },
        // input date with min & max
        {
          name: 'name4',
          type: 'date',
          min: '2017-03-01',
          max: '2018-01-12'
        },
        // input date without min nor max
        {
          name: 'name5',
          type: 'date'
        },
        {
          name: 'name6',
          type: 'number',
          min: -5,
          max: 10
        },
        {
          name: 'name7',
          type: 'number'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Search',
          handler: () => {
            console.log('Confirm Search');
          }
        }
      ]
    });

    await alert.present();
  }
  logout()
	{
		this.authService.logout();
	}
}
