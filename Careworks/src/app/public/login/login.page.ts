import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import {AuthenticationService} from './../../services/authentication.service'
import {AlertController} from '@ionic/angular';
import {LoginForm} from './../../models/User';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  userData=new LoginForm();
  returnUrl: string = '';
  constructor(public menuCtrl: MenuController, private router: Router,private authService:AuthenticationService,
  private alertController:AlertController,private route:ActivatedRoute) {
      
      this.ionViewWillEnter();
  }

   ionViewWillEnter() { 
   		this.menuCtrl.enable(false); 
   	}

  ngOnInit() {
    this.authService.isAuthenticated().then(
      (res)=>{
      if(res){
      this.navigateHome();
      }
    }
    );

    this.route.queryParams
      .subscribe(params => this.returnUrl = params['return'] || '/home');
  }

  Login()
  {
    if(!this.userData.userNameOrEmail)
    {
      this.showAlert("UserNameOrEmail Required");
      return;
    }

    if(!this.userData.password)
    {
      this.showAlert("Password Required");
      return;
    }

    this.authService.Login(this.userData).subscribe(
      ()=>{
        if(this.authService.user['roles'].includes('ROLE_ADMIN'))
        {
          this.showAlert('You are logged in as a Admin');
        }
        else if(this.authService.user['roles'].includes('ROLE_USER'))
        {
          this.showAlert('You are logged in as a user');
        }
        this.router.navigateByUrl(this.returnUrl);
      }
    );
  }

  navigateHome(): void {
	  this.router.navigateByUrl('/home');
  }
  
  showAlert(msg) {
    let alert = this.alertController.create({
      message: msg,
      buttons: ['OK']
    });
    alert.then(alert => alert.present());
  }
  async getToken()
  {
    let token=await this.authService.getToken();
    this.showAlert(token);
  }
  
  async removeToken()
  {
    await this.authService.removeToken();
  }

}
