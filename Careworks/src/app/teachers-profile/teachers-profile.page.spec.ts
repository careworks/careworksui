import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeachersProfilePage } from './teachers-profile.page';

describe('TeachersProfilePage', () => {
  let component: TeachersProfilePage;
  let fixture: ComponentFixture<TeachersProfilePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeachersProfilePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeachersProfilePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
