import { Component, OnInit } from '@angular/core';
import { SchoolProfileService } from '../services/school-profile.service';
import { TeachersProfileModel } from '../models/teachers-profile-model';
import { LocalStoreServiceService } from '../services/local-store-service.service';

@Component({
  selector: 'app-teachers-profile',
  templateUrl: './teachers-profile.page.html',
  styleUrls: ['./teachers-profile.page.scss'],
})
export class TeachersProfilePage implements OnInit {

  parsedschoolDetailsJson = null;
  size = null;
  maleFulltimetotal = 0;
  femaleFulltimetotal = 0;
  maleParttimetotal = 0;
  femaleParttimetotal = 0;
  arttotal = 0;
  petotal = 0;
  comptotal = 0;
  totalTeachers = 0;
  shouldHideTable: any;

  te = new TeachersProfileModel();
  constructor(private schoolProfileService: SchoolProfileService, private localStoreService: LocalStoreServiceService) {
  this.shouldHideTable = true;
  }

  ngOnInit() {
    if (this.localStoreService.isSearchSchoolFlow) {

      this.shouldHideTable = false;

      this.initialisationTextBox();
    }
  }

  initialisationTextBox() {
    this.parsedschoolDetailsJson = this.schoolProfileService.getSampleJson().schoolProfileAdvanced.teacherDetails;
    this.size = this.parsedschoolDetailsJson.length;
    for (let i = 0; i < this.size; i++) {
      // Parsing JSON and fetching the Teacher Count 
      if ((this.parsedschoolDetailsJson[i].teacherType == 'Full-time') && (this.parsedschoolDetailsJson[i].gender == 'Male')) {
        this.maleFulltimetotal += parseInt(this.parsedschoolDetailsJson[i].teacherCount);

      }
      if ((this.parsedschoolDetailsJson[i].teacherType == 'Full-time') && (this.parsedschoolDetailsJson[i].gender == 'Female')) {
        this.femaleFulltimetotal += parseInt(this.parsedschoolDetailsJson[i].teacherCount);

      }
      if ((this.parsedschoolDetailsJson[i].teacherType == 'Part-time') && (this.parsedschoolDetailsJson[i].gender == 'Male')) {
        this.maleParttimetotal += parseInt(this.parsedschoolDetailsJson[i].teacherCount);

      }
      if ((this.parsedschoolDetailsJson[i].teacherType == 'Part-time') && (this.parsedschoolDetailsJson[i].gender == 'Female')) {
        this.femaleParttimetotal += parseInt(this.parsedschoolDetailsJson[i].teacherCount);

      }
      if (this.parsedschoolDetailsJson[i].occupationType == 'Art & Craft') {
        this.arttotal += parseInt(this.parsedschoolDetailsJson[i].teacherCount);
      }

      if (this.parsedschoolDetailsJson[i].occupationType == 'Physical-Education') {
        this.petotal += parseInt(this.parsedschoolDetailsJson[i].teacherCount);
      }
      if (this.parsedschoolDetailsJson[i].occupationType == 'Computer') {
        this.comptotal += parseInt(this.parsedschoolDetailsJson[i].teacherCount);
      }
    }
    this.totalTeachers = this.maleFulltimetotal + this.femaleFulltimetotal + this.maleParttimetotal + this.femaleParttimetotal + this.arttotal + this.petotal + this.comptotal;
    this.te.maleFulltimetotal = this.maleFulltimetotal;
    this.te.femaleFulltimetotal = this.femaleFulltimetotal
    this.te.maleParttimetotal = this.maleParttimetotal;
    this.te.femaleParttimetotal = this.femaleParttimetotal;
    this.te.arttotal = this.arttotal;
    this.te.petotal = this.petotal;
    this.te.comptotal = this.comptotal;
    this.te.totalTeachers = this.totalTeachers;
  }

  saveFunc() {

  }




}
