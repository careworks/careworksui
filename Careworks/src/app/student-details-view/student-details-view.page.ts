import { Component, OnInit } from '@angular/core';
import { SchoolProfileService} from '../services/school-profile.service';

@Component({
  selector: 'app-student-details-view',
  templateUrl: './student-details-view.page.html',
  styleUrls: ['./student-details-view.page.scss'],
})
export class StudentDetailsViewPage implements OnInit {

  parsedJson = null;
  parsedJson1 = null;
  classList = null;
  mediumList = null;
  casteList = null;
  constructor(private schoolProfileService: SchoolProfileService) { }

  ngOnInit() {
    this.classList = this.schoolProfileService.getSampleJson().schoolProfileAdvanced.studentDetails[0].details
    this.mediumList = this.schoolProfileService.getSampleJson().schoolProfileAdvanced.studentDetails

    console.log(this.mediumList)

  }

  


}
