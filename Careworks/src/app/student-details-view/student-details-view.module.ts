import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { StudentDetailsViewPage } from './student-details-view.page';

const routes: Routes = [
  {
    path: '',
    component: StudentDetailsViewPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [StudentDetailsViewPage]
})
export class StudentDetailsViewPageModule {}
