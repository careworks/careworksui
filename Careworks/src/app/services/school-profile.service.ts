import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { LocalStoreServiceService } from '../services/local-store-service.service';

// Typescript custom enum for search types (optional)
export enum SearchType {
  All = '',
  name = 'name',
  city = 'city',
  pincode = 'pincode',
  state = 'state'
}

@Injectable({
  providedIn: 'root'
})

export class SchoolProfileService {

  getSchoolUrl =  'http://13.229.154.187:8080/api/auth/getSchoolProfiles';
  getSchoolByIdUrl = 'http://13.229.154.187:8080/api/auth/getSchoolProfileById';
  addSchoolProfile = ''; 
  apiKey = ''; // <-- Enter your own key here!
 
  /**
   * Constructor of the Service with Dependency Injection
   * @param http The standard Angular HttpClient to make requests
   */
  constructor(private http: HttpClient, private localStoreService: LocalStoreServiceService) { }
 
  /**
  * Get data from the OmdbApi 
  * map the result to return only the results that we need
  * 
  * @param {string} title Search Term
  * @param {SearchType} type movie, series, episode or empty
  * @returns Observable with the search results
  */
  searchData(title: string, type: SearchType): Observable<any> {
    return this.http.get(`${this.getSchoolUrl}?name=${title}`).pipe(
      map(results => {
      console.log('Raw',results);
      return results;
      })
    );
  }

 
  /**
  * Get the detailed information for an ID using the "i" parameter
  * 
  * @param {string} id imdbID to retrieve information
  * @returns Observable with detailed information
  */
  getSchoolDetails(id) {
    return this.http.get(`${this.getSchoolByIdUrl}?school_id=${id}`);
  }
  
 getSampleJson() {

let data = {
	"schoolProfileBasic":
	{
		"schoolId": 10111,
		"name": "Iblur High School",
		"establishedYear": "2000-03-19",
		"location":"urban",
		"address": "Iblur Main Road, Bangalore",
		"schoolCluster":'Cluster A',
		"block":'Block A',
		"region":'Region A',
		"district":"Bangalore",
		"state": "Karnataka",
		"category": 
			{
				"category":"GLPS",
				"description":"Govt. Lower Primary School (1-5th)"
			},
		"type": 
			{
				"description":"Boys",
				"type":"Boys"
			},
		"medium": ["English","Kannada"],
		"diseCode": 202,
		"emailId": "iblurschool@gmail.com",
		"studentCount": 230
	},
	"schoolProfileAdvanced":
	{
		/* This object will always return the number of objects as many as the number of mediums in the school */
		"studentDetails":[
			{
				"medium": "Kannada",
				"details": [
					{
						"class": 1,
						"sectionCount": 1,
						"totalBoys": 50,
						"totalGirls": 60,
						"scboysNumber": 10,
						"scgirlsNumber": 10,
						"stboysNumber": 10,
						"stgirlsNumber": 20
					},
					{
						"class": 2,
						"sectionCount": 1,
						"totalBoys": 50,
						"totalGirls": 60,
						"scboysNumber": 10,
						"scgirlsNumber": 10,
						"stboysNumber": 10,
						"stgirlsNumber": 20
					}
				]
			},
			{
				"medium": "English",
				"details": [
					{
						"class": 1,
						"sectionCount": 1,	
						"totalBoys": 50,
						"totalGirls": 60,
						"scboysNumber": 10,
						"scgirlsNumber": 10,
						"stboysNumber": 10,
						"stgirlsNumber": 20

					}
				]
			}
		],
		"eduDeptFunction": [
			{
				"designation":"HM",
				"name":"Manjula",
				"contactDetails":"9999988888",
				"emailId":"contact@gmail.com"
			},
			{
				"designation":"HMInCharge",
				"name":"Manjunath",
				"contactDetails":"9999988888",
				"emailId":"contact@gmail.com"
			}
		],
		"teacherDetails": [
			{
				"teacherType":"Full-time",
				"occupationType": "Maths",
				"gender":"Female",
				"teacherCount":3
			},
			{
				"teacherType":"Full-time",
				"occupationType": "Science",
				"gender":"Male",
				"teacherCount":6
			},
			{
				"teacherType":"Part-time",
				"occupationType": "Art & Craft",
				"gender":"Male",
				"teacherCount":1
			}
		],
		"stakeholderInfo": [
			{
				"name":"Manjula",
				"memberCount":30,
				"formationYear":"2000-03-19",
				"spocName":"Manjunath",
				"contactNumber":"9999988888"
			},
			{
				"name":"Manjula",
				"memberCount":30,
				"formationYear":"2000-03-19",
				"spocName":"Manjunath",
				"contactNumber":"9999988888"
			}
		],
		/* Any Other Information section is optional now */
		"anyOtherInfo":"sample"
	}	
}
 
return data;

}
}


