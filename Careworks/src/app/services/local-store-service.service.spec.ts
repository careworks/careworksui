import { TestBed } from '@angular/core/testing';

import { LocalStoreServiceService } from './local-store-service.service';

describe('LocalStoreServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LocalStoreServiceService = TestBed.get(LocalStoreServiceService);
    expect(service).toBeTruthy();
  });
});
