import { TestBed } from '@angular/core/testing';

import { SchoolProfileService } from './school-profile.service';

describe('SchoolProfileService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SchoolProfileService = TestBed.get(SchoolProfileService);
    expect(service).toBeTruthy();
  });
});
