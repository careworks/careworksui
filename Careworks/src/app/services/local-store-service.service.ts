import { Injectable } from '@angular/core';
import { SchoolProfileModel } from '../models/school-profile-model'
import { EducationDeptModel } from '../models/education-dept-model'
import { TeachersProfileModel } from '../models/teachers-profile-model'
import { StakeholdersProfileModel } from '../models/stakeholders-profile-model'
import { OtherInfoModel } from '../models/other-info-model'

@Injectable({
  providedIn: 'root'
})
export class LocalStoreServiceService {
	
	private _isSearchSchoolFlow: Boolean = true;

	private _schoolProfileBasic: SchoolProfileModel;
	//private _studentDetails: studentModel;
	private _eduDeptFunction: EducationDeptModel;
	private _teacherDetails: TeachersProfileModel;
	private _stakeholderInfo: StakeholdersProfileModel;
	private _otherInfo: OtherInfoModel;

  constructor() { }

  set isSearchSchoolFlow(searchSchoolFlow: Boolean) {
    this._isSearchSchoolFlow = searchSchoolFlow;
  }

  get isSearchSchoolFlow(): Boolean {
    const tmp: Boolean = this._isSearchSchoolFlow;
    return tmp;
  }

  set schoolProfileBasic(schoolProfile: SchoolProfileModel) {
    this._schoolProfileBasic = schoolProfile;
    console.log(this._schoolProfileBasic.name);
  }

  get schoolProfileBasic(): SchoolProfileModel {
    const tmp: SchoolProfileModel = this._schoolProfileBasic;
    return tmp;
  }


  set eduDeptFunction(eduDept: EducationDeptModel) {
    this._eduDeptFunction = eduDept;
  }

  get eduDeptFunction(): EducationDeptModel {
    const tmp: EducationDeptModel = this._eduDeptFunction;
    return tmp;
  }

  set teacherDetails(teacherDetail: TeachersProfileModel) {
    this._teacherDetails = teacherDetail;
  }

  get teacherDetails(): TeachersProfileModel {
    const tmp: TeachersProfileModel = this._teacherDetails;
    return tmp;
  }

  set stakeholderInfo(stakeholderInformation: StakeholdersProfileModel) {
    this._stakeholderInfo = stakeholderInformation;
  }

  get stakeholderInfo(): StakeholdersProfileModel {
    const tmp: StakeholdersProfileModel = this._stakeholderInfo;
    return tmp;
  }

  set otherInfo(otherInformation: OtherInfoModel) {
    this._otherInfo = otherInformation;
  }

  get otherInfo(): OtherInfoModel {
    const tmp: OtherInfoModel = this._otherInfo;
    return tmp;
  }
}
