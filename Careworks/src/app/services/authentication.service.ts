import { Platform, AlertController } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { environment } from '../../environments/environment';
import { tap, catchError } from 'rxjs/operators';
import {JwtHelperService} from '@auth0/angular-jwt';
import {LoginForm} from './../models/User';
import { Router } from '@angular/router';
const TOKEN_KEY = 'access_token';
 
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
 
  url = environment.url;
  user = null;
  authenticationState:boolean=false;
 
  constructor(private http: HttpClient, private helper: JwtHelperService, private storage: Storage,
    private plt: Platform, private alertController: AlertController,private router:Router) {
    this.plt.ready().then(() => {
    });
  }

   async getToken():Promise<any>
   {
     return this.storage.get(TOKEN_KEY).catch(()=>{console.log("error in getting token")});
   }

   async removeToken()
   {
     return this.storage.remove(TOKEN_KEY).catch(()=>{console.log("error in removing token")});
   }

    async checkIfTokenPresentOrExpired() :Promise<boolean> {
      let token=await this.getToken();
      if(token){
      let decoded=this.helper.decodeToken(token);
      let isExpired=this.helper.isTokenExpired(token);
      if(isExpired)
      {
        await this.removeToken();
        this.authenticationState=false;
        this.showAlert("Your session has expired.Please login again");
      }
      else{
        this.user = decoded;
        this.authenticationState=true;
      }
    }
    else{
      this.authenticationState=false;
    }
    return this.authenticationState;
    }
 
  Login(values:LoginForm) {
    return this.http.post(`${this.url}/api/auth/signin`,values)
      .pipe(
        tap(res => {
          this.storage.set(TOKEN_KEY, res["token"]).then
          (
            ()=>
            {
              console.log("token set succesfully");

            }
          );
          this.user = this.helper.decodeToken(res["token"]);
          this.authenticationState=true;
        }),
        catchError(e => {
        let status = e.status;
        if (status === 401) {
          this.showAlert('Invalid credentials.Please try again');
        }
        throw new Error(e);
        })
      );
  }
 
  logout() {
    this.storage.remove(TOKEN_KEY).then(() => {
      this.authenticationState=false;
      this.router.navigateByUrl('/login');
    });
  }
 
  

  isAuthenticated():Promise<boolean> {
    return this.checkIfTokenPresentOrExpired();
    }
 
  showAlert(msg) {
    let alert = this.alertController.create({
      message: msg,
      buttons: ['OK']
    });
    alert.then(alert => alert.present());
  }
  
}

  