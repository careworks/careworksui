import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherInfoPage } from './other-info.page';

describe('OtherInfoPage', () => {
  let component: OtherInfoPage;
  let fixture: ComponentFixture<OtherInfoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtherInfoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherInfoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
