import { Component, OnInit } from '@angular/core';
import { OtherInfoModel } from '../models/other-info-model';
import { SchoolProfileService} from '../services/school-profile.service';
@Component({
  selector: 'app-other-info',
  templateUrl: './other-info.page.html',
  styleUrls: ['./other-info.page.scss'],
})
export class OtherInfoPage implements OnInit {
parsedschoolDetailsJson = null;
otherInfo = null;
ot = new OtherInfoModel();
  constructor(private schoolProfileService: SchoolProfileService) { }

  ngOnInit() {
	this.initialisationTextBox();
  }

initialisationTextBox(){
this.parsedschoolDetailsJson = this.schoolProfileService.getSampleJson().schoolProfileAdvanced;
this.ot.otherInfo = this.otherInfo = this.parsedschoolDetailsJson.anyOtherInfo;
console.log(this.otherInfo);
}
}
