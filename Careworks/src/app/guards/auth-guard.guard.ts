import {AuthenticationService} from '../services/authentication.service'
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, Route } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable()
export class AuthGuard implements CanActivate {


  constructor(private authService: AuthenticationService, private _router: Router) {
  }

  async canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot):  Promise<boolean>{
    
    if(await this.authService.isAuthenticated())
    {
      return true;
    }
    this._router.navigate(['/login'], {
      queryParams: {
        return: state.url
      }
    });
    return false;
  }

}