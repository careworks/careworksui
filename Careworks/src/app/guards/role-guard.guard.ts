import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../services/authentication.service';


@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivate {
  constructor(private authService:AuthenticationService)
  {

  }
  async canActivate(next: ActivatedRouteSnapshot,state: RouterStateSnapshot): Promise<boolean> {
      await this.authService.isAuthenticated();
      if(this.authService.authenticationState)
      {
        if(this.authService.user["roles"].includes("ROLE_ADMIN"))
        {
          return true;
        }
      }
      return false;
  }
}
