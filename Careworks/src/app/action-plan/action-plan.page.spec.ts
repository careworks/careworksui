import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionPlanPage } from './action-plan.page';

describe('ActionPlanPage', () => {
  let component: ActionPlanPage;
  let fixture: ComponentFixture<ActionPlanPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActionPlanPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionPlanPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
