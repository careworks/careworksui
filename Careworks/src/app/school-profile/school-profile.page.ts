import { Component, OnInit } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { LoadingController, Platform } from '@ionic/angular'
import { SchoolProfileService, SearchType } from '../services/school-profile.service';
import { LocalStoreServiceService } from '../services/local-store-service.service';
import { Observable } from 'rxjs';
import { SchoolProfileModel } from '../models/school-profile-model'

@Component({
  selector: 'app-school-profile',
  templateUrl: './school-profile.page.html',
  styleUrls: ['./school-profile.page.scss'],
})
export class SchoolProfilePage implements OnInit {

  isLoading = false;
  schoolDetails = null;
  parsedschoolDetailsJson = null;
  spm = new SchoolProfileModel();  //this is the getter and setter
  
  constructor(private activatedRoute: ActivatedRoute, private router: Router, private schoolProfileService: SchoolProfileService, public loadingController: LoadingController, private localStoreService: LocalStoreServiceService) { }

  ngOnInit() {
    if(this.localStoreService.isSearchSchoolFlow){
        console.log(this.localStoreService.isSearchSchoolFlow);
        this.present().then(a => console.log('presented'));
        let schoolId = this.activatedRoute.snapshot.paramMap.get('id');
         this.schoolProfileService.getSchoolDetails(schoolId).subscribe(result => {
          console.log('Details',result);
          this.initialisationTextBox(JSON.parse(JSON.stringify(result)).schoolProfileBasic);
          this.dismiss().then(a => console.log('presented'));
        });
    }
    else{
    // this.initialisationTextBox(this.schoolProfileService.getSampleJson().schoolProfileBasic);
    }
    // this.initialisationTextBox(this.schoolProfileService.getSampleJson().schoolProfileBasic);

  }

  initialisationTextBox(parsedJson){
    this.spm.name= parsedJson.name;
    this.spm.location= parsedJson.location;
    this.spm.establishedYear= parsedJson.establishedYear;
    this.spm.address = parsedJson.address;
    this.spm.schoolCluster = parsedJson.schoolCluster;
    this.spm.block = parsedJson.block;
    this.spm.district = parsedJson.district;
    this.spm.state= parsedJson.state;
    if(parsedJson.category != null){
      this.spm.category = parsedJson.category;
    }
    if(parsedJson.type != null){
      this.spm.type = parsedJson.type;
    }
    this.spm.medium = parsedJson.medium;
    this.spm.diseCode = parsedJson.diseCode;
    this.spm.emailId = parsedJson.emailId;
    this.spm.studentCount = parsedJson.studentCount;
    
    // save data to local class
    this.localStoreService.schoolProfileBasic = this.spm;
    //this.localStoreService.
    //
  }

  saveFunc(){
    console.log(this.spm.type);
    this.updateCategoryDescriptioon(this.spm.category.category);
    this.spm.type.description = this.spm.type.type;
    console.log(this.spm);
    this.localStoreService.schoolProfileBasic = this.spm;
    // call the update service here
  }

  updateCategoryDescriptioon(categoryValue:String) {

      switch(categoryValue) { 
         case 'GLPS': { 
            this.spm.category.description = 'Govt. Lower Primary School (1-5th)';
            break; 
         } 
         case 'GHPS': { 
            this.spm.category.description = 'Govt. Higer Primary School (6-8th)';
            break; 
         }
         case 'GMPS': { 
            this.spm.category.description = 'Need to check with Mahak';
            break; 
         } 
         case 'GHS': { 
            this.spm.category.description = 'Need to check with Mahak';
            break; 
         }  
         default: { 
            this.spm.category.description = ''; 
            break; 
         } 
      }

  }

  async present() {
    this.isLoading = true;
    return await this.loadingController.create({
      message: 'Please wait. . .',
      duration: 5000,
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async dismiss() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

}
