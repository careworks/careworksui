import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SchoolProfilePage } from './school-profile.page';

describe('SchoolProfilePage', () => {
  let component: SchoolProfilePage;
  let fixture: ComponentFixture<SchoolProfilePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SchoolProfilePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SchoolProfilePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
