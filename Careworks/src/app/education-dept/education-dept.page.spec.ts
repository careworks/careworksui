import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EducationDeptPage } from './education-dept.page';

describe('StakeholdersProfilePage', () => {
  let component: EducationDeptPage;
  let fixture: ComponentFixture<EducationDeptPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EducationDeptPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EducationDeptPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
