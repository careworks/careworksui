import { Component, OnInit } from '@angular/core';
import { EducationDeptModel } from '../models/education-dept-model';
import { SchoolProfileService } from '../services/school-profile.service';
import { LocalStoreServiceService } from '../services/local-store-service.service';

@Component({
  selector: 'app-education-dept',
  templateUrl: './education-dept.page.html',
  styleUrls: ['./education-dept.page.scss'],
})
export class EducationDeptPage implements OnInit {

  parsedschoolDetailsJson = null;
  size = null;
  hmName = null;
  hmContact = null;
  hmEmail = null;
  hmInChargeName = null;
  hmInChargeContact = null;
  hmInChargeEmail = null;
  crpName = null;
  crpContact = null;
  crpEmail = null;
  brcName = null;
  brcContact = null;
  brcEmail = null;
  beoName = null;
  beoContact = null;
  beoEmail = null;

  edu = new EducationDeptModel();
  private educationdeptlist = [];
  shouldHideTable: any;

  constructor(private schoolProfileService: SchoolProfileService, private localStoreService: LocalStoreServiceService) { this.shouldHideTable = true; }

  ngOnInit() {
    if (this.localStoreService.isSearchSchoolFlow) {
	this.shouldHideTable = false;      
	this.initialisationTextBox();
      
    }

  }

  initialisationTextBox() {
    this.parsedschoolDetailsJson = this.schoolProfileService.getSampleJson().schoolProfileAdvanced.eduDeptFunction;
    this.size = this.parsedschoolDetailsJson.length;
    console.log(this.parsedschoolDetailsJson);
    for (let i = 0; i < this.size; i++) {
      // Parsing JSON and fetching the EduDept Details 
      if (this.parsedschoolDetailsJson[i].designation == 'HM') {
        this.hmName = this.parsedschoolDetailsJson[i].name;
        this.hmContact = this.parsedschoolDetailsJson[i].contactDetails;
        this.hmEmail = this.parsedschoolDetailsJson[i].emailId;
      }
      if (this.parsedschoolDetailsJson[i].designation == 'HMInCharge') {
        this.hmInChargeName = this.parsedschoolDetailsJson[i].name;
        this.hmInChargeContact = this.parsedschoolDetailsJson[i].contactDetails;
        this.hmInChargeEmail = this.parsedschoolDetailsJson[i].emailId;
      }
      if (this.parsedschoolDetailsJson[i].designation == 'CRP') {
        this.crpName = this.parsedschoolDetailsJson[i].name;
        this.crpContact = this.parsedschoolDetailsJson[i].contactDetails;
        this.crpEmail = this.parsedschoolDetailsJson[i].emailId;
      }
      if (this.parsedschoolDetailsJson[i].designation == 'CRP') {
        this.crpName = this.parsedschoolDetailsJson[i].name;
        this.crpContact = this.parsedschoolDetailsJson[i].contactDetails;
        this.crpEmail = this.parsedschoolDetailsJson[i].emailId;
      }
      if (this.parsedschoolDetailsJson[i].designation == 'BRC') {
        this.brcName = this.parsedschoolDetailsJson[i].name;
        this.brcContact = this.parsedschoolDetailsJson[i].contactDetails;
        this.brcEmail = this.parsedschoolDetailsJson[i].emailId;
      }
      if ((this.parsedschoolDetailsJson[i].designation == 'BEO') || (this.parsedschoolDetailsJson[i].designation == 'CEO')) {
        this.beoName = this.parsedschoolDetailsJson[i].name;
        this.beoContact = this.parsedschoolDetailsJson[i].contactDetails;
        this.beoEmail = this.parsedschoolDetailsJson[i].emailId;
      }
    }

    this.edu.hmName = this.hmName;
    this.edu.hmContact = this.hmContact;
    this.edu.hmEmail = this.hmEmail;
    this.edu.hmInChargeName = this.hmInChargeName;
    this.edu.hmInChargeContact = this.hmInChargeContact;
    this.edu.hmInChargeEmail = this.hmInChargeEmail;
    this.edu.crpName = this.crpName;
    this.edu.crpContact = this.crpContact;
    this.edu.crpEmail = this.crpEmail;
    this.edu.brcName = this.brcName;
    this.edu.brcContact = this.brcContact;
    this.edu.brcEmail = this.brcEmail;
    this.edu.beoName = this.beoName;
    this.edu.beoContact = this.beoContact;
    this.edu.beoEmail = this.beoEmail;

  }
  saveFunc() {
    console.log(this.edu.hmName);
    console.log(this.edu.hmContact);
    console.log(this.edu.hmEmail);
  }


  getEducationDeptList() {
    return this.educationdeptlist;
  }

  addEducationDept(educationDept) {
    this.educationdeptlist.push(educationDept);
  }

}
