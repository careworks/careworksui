import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CanActivate } from '@angular/router/src/utils/preactivation';
import {AuthGuard} from '././guards/auth-guard.guard';
import { RoleGuard } from './guards/role-guard.guard';
const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  { path: 'login', 
    loadChildren: './public/login/login.module#LoginPageModule' 
  },
  {
    path: 'home',
    loadChildren: './home/home.module#HomePageModule',
    //canActivate:[AuthGuard]
  },
  {
    path: 'list',
    loadChildren: './list/list.module#ListPageModule',
    //canActivate:[AuthGuard]
  },
  { 
    path: 'school-info', 
    loadChildren: './school-info/school-info.module#SchoolInfoPageModule',
    ///canActivate:[AuthGuard]
  },
  /*{ 
    path: 'school-info/:id', 
    loadChildren: './school-info/school-info.module#SchoolInfoPageModule',
    ///canActivate:[AuthGuard]
  },*/
  { 
    path: 'school-dashboard', loadChildren: './school-dashboard/school-dashboard.module#SchoolDashboardPageModule' 
  },  { path: 'student-details-view', loadChildren: './student-details-view/student-details-view.module#StudentDetailsViewPageModule' }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
