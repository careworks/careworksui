import { Component, OnInit } from '@angular/core';
import { StakeholdersProfileModel } from '../models/stakeholders-profile-model';
import { SchoolProfileService } from '../services/school-profile.service';
import { LocalStoreServiceService } from '../services/local-store-service.service';

@Component({
  selector: 'app-stakeholders-profile',
  templateUrl: './stakeholders-profile.page.html',
  styleUrls: ['./stakeholders-profile.page.scss'],
})
export class StakeholdersProfilePage implements OnInit {
  parsedschoolDetailsJson = null;
  size = null;
  sdmcTotal = null;
  sdmcYear = null;
  sdmcSpoc = null;
  sdmcContact = null;
  alumniTotal = null;
  alumniYear = null;
  alumniSpoc = null;
  alumniContact = null;
  schoolCabinetTotal = null;
  schoolCabinetYear = null;
  schoolCabinetSpoc = null;
  schoolCabinetContact = null;
  parentsTeachersTotal = null;
  parentsTeachersYear = null;
  parentsTeachersSpoc = null;
  parentsTeachersContact = null;
  pocsoTotal = null;
  pocsoYear = null;
  pocsoSpoc = null;
  pocsoContact = null;

  private stakeholdersList = [];

  st = new StakeholdersProfileModel();
  shouldHideTable: any;


  constructor(private schoolProfileService: SchoolProfileService, private localStoreService: LocalStoreServiceService) {
  this.shouldHideTable = true;
  }

  ngOnInit() {
    if (this.localStoreService.isSearchSchoolFlow) {

      this.shouldHideTable = false;
      this.initialisationTextBox();
    }
  }
  initialisationTextBox() {
    this.parsedschoolDetailsJson = this.schoolProfileService.getSampleJson().schoolProfileAdvanced.stakeholderInfo;
    this.size = this.parsedschoolDetailsJson.length;
    console.log(this.parsedschoolDetailsJson[0]);

    // Can iterate over the list at later stage

    this.sdmcTotal = parseInt(this.parsedschoolDetailsJson[0].memberCount);
    this.sdmcYear = this.parsedschoolDetailsJson[0].formationYear.substr(0, 4);
    this.sdmcSpoc = this.parsedschoolDetailsJson[0].spocName;
    this.sdmcContact = this.parsedschoolDetailsJson[0].contactNumber;
    this.alumniTotal = parseInt(this.parsedschoolDetailsJson[1].memberCount);
    this.alumniYear = this.parsedschoolDetailsJson[1].formationYear.substr(0, 4);
    this.alumniSpoc = this.parsedschoolDetailsJson[1].spocName;
    this.alumniContact = this.parsedschoolDetailsJson[1].contactNumber;

    //Mapping to Model Fields
    this.st.sdmcTotal = this.sdmcTotal;
    this.st.sdmcYear = this.sdmcYear;
    this.st.sdmcSpoc = this.sdmcSpoc;
    this.st.sdmcContact = this.sdmcContact;
    this.st.alumniTotal = this.alumniTotal;
    this.st.alumniSpoc = this.alumniSpoc;
    this.st.alumniContact = this.alumniContact;
    this.st.alumniYear = this.alumniYear;
    this.st.schoolCabinetTotal = this.schoolCabinetTotal;
    this.st.schoolCabinetSpoc = this.schoolCabinetSpoc;
    this.st.schoolCabinetYear = this.schoolCabinetYear;
    this.st.schoolCabinetContact = this.schoolCabinetContact;
    this.st.parentsTeachersTotal = this.parentsTeachersYear;
    this.st.parentsTeachersSpoc = this.parentsTeachersSpoc;
    this.st.parentsTeachersContact = this.parentsTeachersContact;
    this.st.pocsoTotal = this.pocsoTotal;
    this.st.pocsoYear = this.pocsoYear;
    this.st.pocsoSpoc = this.pocsoSpoc;
    this.st.pocsoContact = this.pocsoContact;


  }


  saveFunc() {
    console.log(this.st.sdmcTotal);
    console.log(this.st.sdmcYear);
  }


  getStakeholdersList() {
    return this.stakeholdersList;
  }

  addStakeholder(stakeholder) {
    this.stakeholdersList.push(stakeholder);
  }

}
