import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StakeholdersProfilePage } from './stakeholders-profile.page';

describe('StakeholdersProfilePage', () => {
  let component: StakeholdersProfilePage;
  let fixture: ComponentFixture<StakeholdersProfilePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StakeholdersProfilePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StakeholdersProfilePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
