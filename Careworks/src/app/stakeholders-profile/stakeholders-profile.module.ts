import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { StakeholdersProfilePage } from './stakeholders-profile.page';

const routes: Routes = [
  {
    path: '',
    component: StakeholdersProfilePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [StakeholdersProfilePage]
})
export class StakeholdersProfilePageModule {}
