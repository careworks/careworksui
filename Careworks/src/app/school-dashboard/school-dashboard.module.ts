import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SchoolDashboardPage } from './school-dashboard.page';

const routes: Routes = [
  {
    path: '',
    component: SchoolDashboardPage,
    children: [
      { 
      path: 'need-assessment', loadChildren: '../need-assessment/need-assessment.module#NeedAssessmentPageModule' 
      },
      { 
      path: 'action-plan', loadChildren: '../action-plan/action-plan.module#ActionPlanPageModule' 
      },
      { 
      path: 'monitorring', loadChildren: '../monitorring/monitorring.module#MonitorringPageModule' 
      }
    ]
  },
  {
    path: '',
    redirectTo: '/need-assessment',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SchoolDashboardPage]
})
export class SchoolDashboardPageModule {}
