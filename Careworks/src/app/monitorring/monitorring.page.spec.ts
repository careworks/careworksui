import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonitorringPage } from './monitorring.page';

describe('MonitorringPage', () => {
  let component: MonitorringPage;
  let fixture: ComponentFixture<MonitorringPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonitorringPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonitorringPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
